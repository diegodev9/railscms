class AddFeaturedToPages < ActiveRecord::Migration[6.1]
  def change
    add_column :pages, :featured, :boolean
  end
end
