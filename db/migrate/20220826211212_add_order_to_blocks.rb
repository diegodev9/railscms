class AddOrderToBlocks < ActiveRecord::Migration[6.1]
  def change
    add_column :blocks, :order, :integer
  end
end
