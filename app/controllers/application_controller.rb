class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :get_page_nav, :get_jumbotron, :get_blocks

  def get_page_nav
    @page_nav = Page.where(menu_display: true, is_published: true).order(order: :asc)
  end

  def get_jumbotron
    @jumbotron = Block.where(position: 'jumbotron')
  end

  def get_blocks
    @allblocks = Block.where(position: 'block').order(order: :asc)
  end
end
