class HomeController < ApplicationController
  def show
    @root = 'active'
    @pages = Page.where(featured: true)
  end
end
