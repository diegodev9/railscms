class PagesController < ApplicationController
  def show
    @page = Page.find(params[:id])
    redirect_to root_path, alert: "This page does not exist" if @page.is_published == false

    @sections = Section.all
  end
  console
end
